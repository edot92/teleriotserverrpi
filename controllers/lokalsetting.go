package controllers

import (
	"net/http"
	"strings"

	"gitlab.com/edot92/teleriotserverrpi/libgo"

	"github.com/gin-gonic/gin"
)

// SetAuth
// set lokal konfigurasi
func SetAuth(c *gin.Context) {
	type paramBody struct {
		Token      string `json:"token"`
		HardwareID string `json:"hardwareId"`
	}
	var paramJSON paramBody
	if err := c.ShouldBindJSON(&paramJSON); err == nil {
		if paramJSON.HardwareID != libgo.DiskSerialNumber {
			c.JSON(http.StatusOK, gin.H{
				"error": 1,
				"msg":   "failed parameter",
			})
			return
		}
		var modelTB libgo.TbKeys
		modelTB.SerialNumber = libgo.DiskSerialNumber
		err2 := libgo.DB.Table("tb_keys").Delete(nil).GetErrors()
		if len(err2) == 0 {

		} else {
			c.JSON(http.StatusOK, gin.H{
				"error": 1,
				"msg":   string(err2[0].Error()),
			})
			return
		}
		paramJSON.Token = strings.Replace(paramJSON.Token, " ", "", -1)
		modelTB.DeviceID = paramJSON.Token
		err2 = libgo.DB.Create(&modelTB).GetErrors()
		if len(err2) == 0 {
			// diskonek server
			c.JSON(http.StatusOK, gin.H{
				"error": 0,
				"msg":   "success update",
			})
			libgo.InitsocketClient()
		} else {
			c.JSON(http.StatusOK, gin.H{
				"error": 1,
				"msg":   string(err2[0].Error()),
			})
		}
	} else {
		c.JSON(http.StatusOK, gin.H{"msg": "failed parameter", "error": 1})
	}
}
