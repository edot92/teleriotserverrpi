package router

import (
	"log"

	"github.com/googollee/go-socket.io"
)

func initSocket() *socketio.Server {
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Println(err)
		log.Fatal(err)
	}
	server.On("connection", func(so socketio.Socket) {
		// log.Println("on connection")
		so.On("join", func(msg string) string {
			log.Println("join to room " + msg)
			// so.Join("roomclientapp")
			so.Join(msg)
			return "OK join to room"
		})
		so.On("disconnection", func() {
			log.Println("on disconnect")
		})

	})
	server.On("error", func(so socketio.Socket, err error) {
		log.Println("error:", err)
	})
	return server
	// GinRouter.GET("/socket.io/", ginw.WrapHH(server))
	// server.ServeHTTP(c.Writer, c.Request)

	// http.Handle("/socket.io/", server)
	// http.Handle("/", http.FileServer(http.Dir("./asset")))

}
