package router

import (
	"html/template"
	"io/ioutil"
	"net/http"
	"time"

	gintemplate "github.com/foolin/gin-template"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"gitlab.com/edot92/teleriotserverrpi/controllers"
	"gitlab.com/edot92/teleriotserverrpi/libgo"
)

func init() {

}

var GinRouter *gin.Engine

func InitRouter() *gin.Engine {
	// http handler
	gin.SetMode(gin.ReleaseMode)
	gin.DefaultWriter = ioutil.Discard
	r := gin.Default()
	// r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(cors.Default())
	r.Static("/assets", "./assets")
	//new template engine
	r.HTMLRender = gintemplate.New(gintemplate.TemplateConfig{
		Root:      "templates",
		Extension: ".html",
		Master:    "layouts/master",
		Partials:  []string{},
		Funcs: template.FuncMap{
			"sub": func(a, b int) int {
				return a - b
			},
			"copy": func() string {
				return time.Now().Format("2006")
			},
		},
		DisableCache: true,
	})
	r.GET("/", func(ctx *gin.Context) {
		var modelTB libgo.TbKeys
		modelTB.SerialNumber = libgo.DiskSerialNumber
		// var deviceId interface{}
		err := libgo.DB.Table("tb_keys").First(&modelTB,
			libgo.TbKeys{
				SerialNumber: libgo.DiskSerialNumber,
			},
		).Error
		if err == nil {
			libgo.DeviceID = modelTB.DeviceID
			ctx.HTML(http.StatusOK, "page/index", gin.H{
				"title":        "Lokal Konfigurasi",
				"deviceID":     modelTB.DeviceID,
				"serialNumber": libgo.DiskSerialNumber,
				"baseUrl":      (ctx.Request.Host),
			})
		} else {
			ctx.HTML(http.StatusOK, "page/index", gin.H{
				"title":        "Lokal Konfigurasi",
				"deviceID":     "",
				"serialNumber": libgo.DiskSerialNumber,
				"baseUrl":      (ctx.Request.Host),
			})
		}

	})
	r.Use(static.Serve("/", static.LocalFile("dist", true)))
	r.Use(static.Serve("/static", static.LocalFile("dist/static", true)))
	// r.GET("/socket.io", initSocket)
	// r.POST("/socket.io", initSocket)
	libgo.ServerSocket = initSocket()
	r.GET("/socket.io/", gin.WrapH(libgo.ServerSocket))
	r.POST("/socket.io/", gin.WrapH(libgo.ServerSocket))

	v1 := r.Group("/api/v1/lokalsetting")
	{
		v1.POST("/SetAuth", controllers.SetAuth)
	}
	// interval.Start(func() {
	// 	libgo.ServerSocket.BroadcastTo("roomclientapp", "logclientapp", "tes online")
	// }, 5*time.Second)
	// r.Handle("WSS", "/socket.io", []gin.WrapF{initSocket})

	// r.GET("/ws", func(c *gin.Context) {
	// 	// CHECK AUTH HEADER HERE
	// 	// Open the socket...  this or whatever other way you are using.
	// 	handler := serverSocket
	// 	handler.ServeHTTP(c.Writer, c.Request)
	// })
	// r.GET("/wss", func(c *gin.Context) {
	// 	// CHECK AUTH HEADER HERE
	// 	// Open the socket...  this or whatever other way you are using.
	// 	handler := serverSocket
	// 	handler.ServeHTTP(c.Writer, c.Request)
	// })
	// r.Use(initSocket)
	// r.Handle(
	// 	"WS", "/socket.io",
	// 	initSocket,
	// )
	// r.Handle(
	// 	"WSS", "/socket.io",
	// 	initSocket,
	// )
	// r.Handle("WS", "/socket.io", []gin.HandlerFunc{initSocket})
	// r.Handle("WSS", "/socket.io", []gin.HandlerFunc{initSocket})
	GinRouter = r
	InitCors()
	return r
}
