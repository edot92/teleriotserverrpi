package main

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/aubm/interval"
	"github.com/edot92/fmtdate"
	"github.com/gin-gonic/gin"
	"github.com/shirou/gopsutil/cpu"

	_ "github.com/go-ini/ini"
	"github.com/goburrow/modbus"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gitlab.com/edot92/teleriotserverrpi/libgo"
	"gitlab.com/edot92/teleriotserverrpi/router"
	ini "gopkg.in/ini.v1"
)

type structEnergyMeter struct {
	Token      string `json:"token"`
	HardwareID string `json:"hardwareId"`
	Tegangan   string `json:"tegangan"`
	Arus       string `json:"arus"`
	DayaAktif  string `json:"dayaAktif"`
	Daya       string `json:"daya"`
	Frekuensi  string `json:"frekuensi"`
}

var register [15]uint16
var errModbus error
var dataEnergyMeter structEnergyMeter

func readKonfigurasiFile() (string, error) {
	cfg, err := ini.InsensitiveLoad("setting.ini")
	if err != nil {
		return "", err
	}
	key, err := cfg.Section("").GetKey("PORTUSB")
	if err != nil {
		return "", err
	}
	return key.String(), nil
}

var SERVERVPS string
var PORTVPS int

func main() {
	var flagSendNotif = false
	stop := interval.Start(func() {
		flagSendNotif = true
	}, 60*time.Second)
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Println("start")
	libgo.InitDb()
	awal, _ := fmtdate.NewTimeDate("DD-MM-YYYY hh:mm:ss", "01-05-2018 00:00:00")
	akhir, _ := fmtdate.NewTimeDate("DD-MM-YYYY hh:mm:ss", "30-05-2018 23:59:59")
	libgo.GetKwh(awal.Time, akhir.Time)
	cpuk, err := cpu.Info()
	// onluy when arm /rpi
	if err == nil {
		model := strings.ToLower(cpuk[0].ModelName)
		if strings.Contains(model, "arm") {
			libgo.RpiSetOn()
		}
	} else {
		log.Println(err)
	}
	_ = cpuk
	portWeb := os.Getenv("PORT")

	if portWeb == "" {
		portWeb = "9000"
	}
	r := router.InitRouter()
	r.Use(gin.Recovery())

	go http.ListenAndServe(":"+portWeb, r)
	if err != nil {
		log.Fatal(err)
	}
	// cfg, err := ini.InsensitiveLoad("setting.ini")

	// key1, err := cfg.Section(libgo.IniSection).GetKey("SERVERVPS")
	// if err == nil {
	// 	SERVERVPS = key1.String()
	// 	log.Println(SERVERVPS)
	// }
	// key1, err = cfg.Section(libgo.IniSection).GetKey("PORTVPS")
	// if err == nil {
	// 	PORTVPS, _ = key1.Int()
	// 	log.Println(PORTVPS)
	// }
konekSocket:
	_, err = libgo.InitsocketClient()
	if err != nil {
		log.Println(err)
		time.Sleep(5 * time.Second)
		goto konekSocket
	}
	go func() {
		countLimitKwh := 0
		countMaxLimitKwh := 8
		_ = countMaxLimitKwh
		for {
			// cek scoket agar tetap terkoneksi
			time.Sleep(5 * time.Second)
			isAlive := libgo.IoClient.IsAlive()
			if isAlive == false {
				_, err := libgo.InitsocketClient()
				if err != nil {
					log.Println(err)
				}
			} else {
				libgo.SockeClientSend("browser_ping_dari_alat", libgo.DeviceID)
				// pengecekan limit kwh

				if flagSendNotif == true && libgo.LimitKwh > 0 {
					flagSendNotif = false
					//log.Print("limit kwh=")
					//log.Println(libgo.LimitKwh)
					log.Println("trigger batas kwh ")
					// do cek database
					// get bulan dan tahun untuk sum range waktu min dan max xselama 1 bulam
					bulan := fmtdate.Format("MM", time.Now())
					tahun := fmtdate.Format("YYYY", time.Now())
					awal, _ := fmtdate.NewTimeDate("DD-MM-YYYY hh:mm:ss", "01-"+bulan+"-"+tahun+" 00:00:00")
					var isErrorDate = true
					akhir, errDate := fmtdate.NewTimeDate("DD-MM-YYYY hh:mm:ss", "31-"+bulan+"-"+tahun+" 23:59:59")
					var akhir2 fmtdate.TimeDate
					if errDate != nil {
						for index2 := 0; index2 < 3; index2++ {
							if index2 == 0 {
								akhir2, errDate = fmtdate.NewTimeDate("DD-MM-YYYY hh:mm:ss", "31-"+bulan+"-"+tahun+" 23:59:59")
							} else if index2 == 1 {
								akhir2, errDate = fmtdate.NewTimeDate("DD-MM-YYYY hh:mm:ss", "30-"+bulan+"-"+tahun+" 23:59:59")
							} else {
								akhir2, errDate = fmtdate.NewTimeDate("DD-MM-YYYY hh:mm:ss", "29-"+bulan+"-"+tahun+" 23:59:59")
							}
							if errDate != nil {
								log.Println(err)
							} else {
								isErrorDate = false
								break
							}
						}
						if isErrorDate == true {
							log.Println("date error")
						} else {
							akhir.Time = akhir2.Time
						}

					} else {
						isErrorDate = false
					}
					log.Println(awal)
					log.Println(akhir)
					actualKwhStr, _ := libgo.GetKwh(awal.Time, akhir.Time)
					actualKwhFloat, _ := strconv.ParseFloat(actualKwhStr, 64)
					// jika sudah mencapai limit, send notif ke telegram
					if actualKwhFloat > float64(libgo.LimitKwh) {
						event := "notif_dari_alat"
						log.Println("kirim notifikasi")
						limitKehStr := strconv.Itoa(libgo.LimitKwh)
						msg := "Pemakaian daya pada bulan " + bulan + " " + tahun + " , Sudah melebihi  " + limitKehStr + " kwh. Pemakaian terdeteksi sebesar " + actualKwhStr + " kwh "
						libgo.SockeClientSend(event, libgo.DeviceID+"(-_-)"+msg)
					}
					countLimitKwh = 0
				} else {
					countLimitKwh++
				}
			}

		}
	}()
	defer libgo.IoClient.Close()
	// thread serial modbus run
	countToSave := 10 //set default to save karenval =max
	maxCountToSave := 10

	// ...

	// go func() {
	for {
	scanModbus:
		time.Sleep(2 * time.Second)
		COMPORT, err := readKonfigurasiFile()
		// log.Println("scan modbus")
		// log.Println(COMPORT)
		if err != nil {
			libgo.ServerSocket.BroadcastTo("roomclientapp", "logclientapp", err.Error())
			time.Sleep(3 * time.Second)
			errModbus = err
			goto scanModbus
		}
		// Modbus RTU/ASCII
		handler := modbus.NewRTUClientHandler(COMPORT)
		handler.BaudRate = 9600
		handler.DataBits = 8
		handler.Parity = "N"
		handler.StopBits = 1
		handler.SlaveId = 1
		handler.Timeout = 5 * time.Second
		err = handler.Connect()
		if err != nil {
			if strings.Contains(err.Error(), "no such file or directory") {
				err = errors.New("KONEKSI PORT :" + COMPORT + " TIDAK SESUAI , silakan ubah konfigurasi COM PORT")
			} else if strings.Contains(err.Error(), "timeout") {
				handler.Close()
				err = errors.New("Tidak ada respon dari alat , silakan cek konfigurasi COM PORT , dan koneksi ke power meter")
			}
			errModbus = err
			libgo.ServerSocket.BroadcastTo("roomclientapp", "logclientapp", err.Error())
			log.Println(errModbus)
			time.Sleep(3 * time.Second)
			goto scanModbus
		} else {
			libgo.ServerSocket.BroadcastTo("roomclientapp", "logclientapp", "sukses reading")
		}
		defer handler.Close()
		client := modbus.NewClient(handler)
		resModbusByte, err := client.ReadHoldingRegisters(0, 20)
		if err != nil {
			errModbus = err
			log.Println(err.Error())
			libgo.ServerSocket.BroadcastTo("roomclientapp", "logclientapp", err.Error())
			handler.Close()
			goto scanModbus
		}
		dayaAktif := float64(binary.BigEndian.Uint16(resModbusByte[0:4])) * 0.1
		tegangan := float64(binary.BigEndian.Uint16(resModbusByte[4:6])) * 0.01
		arus := float64(binary.BigEndian.Uint16(resModbusByte[6:10])) * 0.001
		daya := float64(binary.BigEndian.Uint16(resModbusByte[10:13])) * 0.01
		// log.Println(resModbusByte)
		// frekuensi := float64(binary.BigEndian.Uint16(resModbusByte[24:25])) * 0.01
		dataEnergyMeter.DayaAktif = fmt.Sprintf("%2f", dayaAktif)
		dataEnergyMeter.Tegangan = fmt.Sprintf("%2f", tegangan)
		dataEnergyMeter.Arus = fmt.Sprintf("%2f", arus)
		dataEnergyMeter.Daya = fmt.Sprintf("%2f", daya)
		if libgo.DeviceID == "" {
			dataEnergyMeter.Token = "ini token sementara"
		} else {
			dataEnergyMeter.Token = libgo.DeviceID
		}
		dataEnergyMeter.HardwareID = libgo.DiskSerialNumber
		// save todb sampai max yang di tentukan
		//log.Println(countToSave)
		//log.Println(maxCountToSave)
		if countToSave >= maxCountToSave {
			tbLogs := libgo.TBLogs{
				DayaAktif: dataEnergyMeter.DayaAktif,
				Tegangan:  dataEnergyMeter.Tegangan,
				Arus:      dataEnergyMeter.Arus,
				Daya:      dataEnergyMeter.Daya,
				Waktu:     time.Now(),
			}
			errSaveLog := libgo.SaveLog(tbLogs)
			if errSaveLog != nil {
				log.Println("error when save db , code" + errSaveLog.Error())
			}
			countToSave = 0
		} else {
			countToSave++
		}

		if libgo.IoClient.IsAlive() {
			// libgo.IoClient.Channel.BroadcastTo("chat", "message", "tes")
			var dataSend []byte
			dataSend, _ = json.Marshal(dataEnergyMeter)
			err := libgo.SocketSendDataModbus(dataSend)
			if err != nil {
				// log.Println("failed to send socket")
				libgo.ServerSocket.BroadcastTo("roomclientapp", "logclientapp", "failed to send socket")
			} else {
				// log.Println("succces send socket")
				libgo.ServerSocket.BroadcastTo("roomclientapp", "logclientapp", "succces update server")
			}
		}
		handler.Close()
		time.Sleep(2 * time.Second)
	}
	// }()

	/* endless loop */
	// for {
	// 	time.Sleep(100 * time.Millisecond)
	// }
	stop() // stops the interval

	log.Println("exit")
}
