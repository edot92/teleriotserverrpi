// +build linux

package libgo

import (
	"log"
	"strings"

	"github.com/shirou/gopsutil/cpu"
	rpio "github.com/stianeikeland/go-rpio"
)

func init() {

	// only when arm /rpi
	cpuk, err := cpu.Info()
	if err == nil {
		model := strings.ToLower(cpuk[0].ModelName)
		if strings.Contains(model, "arm") {
			err = rpio.Open()
			if err == nil {
				pin1 := rpio.Pin(19)
				log.Println("raspberry")
				pin1.Output() // Output mode
				pin1.Low()    // Set pin High
				pin2 := rpio.Pin(26)
				log.Println("raspberry")
				pin2.Output() // Output mode
				pin2.Low()    // Set pin High
				defer rpio.Close()
			} else {
				log.Println(err)
			}
		} else {
			log.Println("not rpi")
		}
	} else {
		log.Println(err)
	}
}
func RpiSetOn() {
	cpuk, err := cpu.Info()
	if err == nil {
		model := strings.ToLower(cpuk[0].ModelName)
		if strings.Contains(model, "arm") {
			err = rpio.Open()
			if err == nil {
				pin1 := rpio.Pin(19)
				pin1.Output() // Output mode
				pin2 := rpio.Pin(26)
				pin2.Output() // Output mode
				pin1.High()   // Set pin High
				pin2.High()   // Set pin High
				defer rpio.Close()
			} else {
				log.Println(err)
			}
		} else {
			log.Println("not arm")
		}
	} else {
		log.Println(err)
	}
}

func RpiSetOff() {
	cpuk, err := cpu.Info()
	if err == nil {
		model := strings.ToLower(cpuk[0].ModelName)
		if strings.Contains(model, "arm") {
			err = rpio.Open()
			if err == nil {
				pin1 := rpio.Pin(19)
				pin1.Output() // Output mode
				pin2 := rpio.Pin(26)
				pin2.Output() // Output mode
				pin1.Low()    // Set pin Low
				pin2.Low()    // Set pin High
				defer rpio.Close()
			} else {
				log.Println(err)
			}
		} else {
			log.Println("not arm")
		}
	} else {
		log.Println(err)
	}
}
