package libgo

import (
	"log"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func init() {

}

type TbKeys struct {
	gorm.Model
	DeviceID     string `gorm:"type:varchar(100)"`
	SerialNumber string `gorm:"type:varchar(100)"`
}
type TBLogs struct {
	Tegangan  string    `json:"tegangan"`
	Arus      string    `json:"arus"`
	DayaAktif string    `json:"dayaAktif"`
	Daya      string    `json:"daya"`
	Frekuensi string    `json:"frekuensi"`
	Waktu     time.Time `json:"waktu"`
}

var DB *gorm.DB

func InitDb() *gorm.DB {
	db, err := gorm.Open("sqlite3", "telkombot.db")
	if err != nil {
		log.Fatal("failed to connect database")
	}
	// defer db.Close()
	// Migrate the schema
	db.AutoMigrate(&TbKeys{})
	db.AutoMigrate(&TBLogs{})
	DB = db
	// set device id
	var modelTB TbKeys
	modelTB.SerialNumber = DiskSerialNumber
	// var deviceId interface{}
	err = DB.Table("tb_keys").First(&modelTB,
		TbKeys{
			SerialNumber: DiskSerialNumber,
		},
	).Error
	if err == nil {
		DeviceID = modelTB.DeviceID
	}
	return db
}
