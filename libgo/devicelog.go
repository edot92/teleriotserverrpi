package libgo

import (
	// "fmt"
	 "log"
	"strconv"
	"time"
)

func init() {

}

func SaveLog(val TBLogs) error {
	var err error
	err = DB.Create(&val).Error
	log.Println("save kwh ")
	
	return err
}

// GetKwh
// get data kwh meter berdsarkan tanggal awal dan akhir,
// return nilai kwh
func GetKwh(dateFrom *time.Time, dateTo *time.Time) (kwh string, err error) {
	kwh = "0"
	var ValAwal TBLogs
	var ValAkhir TBLogs
	err1 := DB.Where("waktu BETWEEN  (?) AND (?) ", dateFrom, dateTo).Order("waktu asc").First(&ValAwal).Error
	err2 := DB.Where("waktu BETWEEN  (?) AND (?) ", dateFrom, dateTo).Order("waktu desc").First(&ValAkhir).Error
	if err1 != nil {
		return kwh, err1
	}
	if err2 != nil {
		return kwh, err2
	}
	//log.Println(ValAwal.DayaAktif)
	//log.Println("----------------------")
	//log.Println(ValAkhir.DayaAktif)
	fAwal, _ := strconv.ParseFloat(ValAwal.DayaAktif, 64)
	fAkhir, _ := strconv.ParseFloat(ValAkhir.DayaAktif, 64)
	valFloatTotal := (fAkhir - fAwal)
	fStringTotal := strconv.FormatFloat(valFloatTotal, 'f', 6, 64)
	//fmt.Println(fStringTotal)
	//fmt.Println(valFloatTotal)
	kwh = fStringTotal
	return kwh, err

}
