package libgo

import (
	"log"

	ini "gopkg.in/ini.v1"
)

var IniSection string
var IniCfg *ini.File

func init() {
	cfg, err := ini.InsensitiveLoad("setting.ini")
	if err != nil {
		log.Fatal(err)
	}
	key1, err := cfg.Section("").GetKey("VPS")
	isVps, _ := key1.Bool()
	if isVps {
		IniSection = "prod"
	} else {
		IniSection = "dev"
	}
	IniCfg = cfg
}
