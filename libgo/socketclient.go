package libgo

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"strconv"
	"strings"

	valid "github.com/asaskevich/govalidator"

	"github.com/edot92/fmtdate"
	gosocketio "github.com/graarh/golang-socketio"
	"github.com/graarh/golang-socketio/transport"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/host"
)

// socket client
// request ke server untuk status alat
// serta untuk online dan offline

var IoClient *gosocketio.Client
var LimitKwh int

func init() {
	// baca konfigurasi limit kwh jika ada
	dat, err := ioutil.ReadFile("bataskwh")
	if err == nil {
		valStr := string(dat)
		if valid.IsInt(valStr) == true && valStr != "" {
			valInt, _ := strconv.Atoi(valStr)
			LimitKwh = valInt
		}
	}
}

type teleFormatCmd1 struct {
	NamaPerangkat string `json:"namaPerangkat"`
	WaktuAwal     string `json:"waktuAwal"`
	WaktuAkhir    string `json:"waktuAkhir"`
}
type teleFormatCmd2 struct {
	NamaPerangkat string `json:"namaPerangkat"`
	BatasKwh      string `json:"batasKwh"`
}
type teleFormatCmd3 struct {
	NamaPerangkat string `json:"namaPerangkat"`
	Command       string `json:"command"`
}

func InitsocketClient() (*gosocketio.Client, error) {
	// cek apakah sudah offline
	if IoClient != nil {
		if IoClient.IsAlive() {
			IoClient.Close()
		}
	}
	SERVERVPS := ""
	PORTVPS := 9999
	key1, err := IniCfg.Section(IniSection).GetKey("SERVERVPS")
	if err == nil {
		SERVERVPS = key1.String()
		// fmt.Println(SERVERVPS)
	}
	key1, err = IniCfg.Section(IniSection).GetKey("PORTVPS")
	if err == nil {
		PORTVPS, _ = key1.Int()
		// fmt.Println(PORTVPS)
	}

	//connect to server, you can use your own transport settings
	portvpsString := strconv.Itoa(PORTVPS)
	log.Println("coba konek soket" + "  SERVERVPS :" + SERVERVPS + "  PORT " + portvpsString)
	if DeviceID == "" {
		return nil, errors.New("Device id tidak ada")
	}
	c, err := gosocketio.Dial(
		gosocketio.GetUrl(SERVERVPS, PORTVPS, false),
		transport.GetDefaultWebsocketTransport(),
	)
	if err != nil {
		fmt.Println(err.Error() + "  SERVERVPS :" + SERVERVPS + "  PORT " + portvpsString)
		return nil, err
	}

	c.On(gosocketio.OnConnection, func(h *gosocketio.Channel) {
		DeviceID = strings.Replace(DeviceID, " ", "", -1)
		err = h.Emit("server_join_room", DeviceID)
		if err != nil {
			log.Println("error when join")
			c.Close()
		} else {
			log.Println("join to  server golang : room " + DeviceID)
			// c.Join(DeviceID)

		}
	})
	c.On("browser_cmd", func(h *gosocketio.Channel, args interface{}) {
		// log.Print("cmd from browser ")
		if valCmd, isOk := args.(string); isOk {
			// log.Println(ghw.CPUInfo.String())
			// gopsutil.os
			platform, family, version, err := host.PlatformInformation()
			cpuk, _ := cpu.Info()
			model := strings.ToLower(cpuk[0].ModelName)
			if err == nil {
				if strings.Contains(model, "arm") {
					// log.Print(" raspberry , cmd :")
					// log.Println(valCmd)
					valCmd1 := strings.ToLower(valCmd)
					if valCmd1 == "on" {
						RpiSetOn()
					} else if valCmd1 == "off" {
						RpiSetOff()
					} else {
						log.Println("cmd not found")
					}
				} else {
					log.Print("not raspberry , cmd :")
					log.Println(valCmd)
				}
				// log.Println(platform)
				// log.Println(family)
				// log.Println(version)
			}
			_ = platform
			_ = family
			_ = version
		}
	})
	c.On("tele_cmd", func(h *gosocketio.Channel, msgTele interface{}) string {
		log.Print("tele_cmd")
		msgTele1 := msgTele.(string)
		log.Println(msgTele1)
		resp, _ := teleParseMsg(msgTele1)
		log.Println(msgTele)
		log.Println("respon:")
		log.Println(resp)
		return resp
	})
	c.On("tele_cmd_1", func(h *gosocketio.Channel, msgTele interface{}) string {
		log.Print("tele_cmd_1")
		var teleFormat teleFormatCmd1
		log.Println(msgTele)
		msgByte := msgTele.(map[string]interface{})
		teleFormat.NamaPerangkat = msgByte["namaPerangkat"].(string)
		teleFormat.WaktuAwal = msgByte["waktuAwal"].(string)
		teleFormat.WaktuAkhir = msgByte["waktuAkhir"].(string)
		awal, _ := fmtdate.NewTimeDate("DD-MM-YYYY hh:mm:ss", teleFormat.WaktuAwal+" 00:00:00")
		akhir, _ := fmtdate.NewTimeDate("DD-MM-YYYY hh:mm:ss", teleFormat.WaktuAkhir+" 23:59:59")
		valKwh, err := GetKwh(awal.Time, akhir.Time)
		var resp string
		if err != nil {
			if strings.Contains(err.Error(), "not found") {
				resp = "belum ada data terekam di perangkat lokal, code " + err.Error()
			} else {
				resp = "Kesalahan di lokal perangkat untuk perintah cek pemakaian daya , code " + err.Error()
			}
		} else {
			resp = "Pemakaian daya listrik untuk perangkat " + teleFormat.NamaPerangkat + " dari tanggal " + teleFormat.WaktuAwal + " s/d " + teleFormat.WaktuAkhir + " adalah sebesar " + valKwh + " kwh"
		}
		return resp
	})
	c.On("tele_cmd_2", func(h *gosocketio.Channel, msgTele interface{}) string {
		var teleFormat teleFormatCmd2
		log.Println(msgTele)
		msgByte := msgTele.(map[string]interface{})
		teleFormat.NamaPerangkat = msgByte["namaPerangkat"].(string)
		teleFormat.BatasKwh = msgByte["batasKwh"].(string)
		namaPerangkat := teleFormat.NamaPerangkat
		batasKwh := teleFormat.BatasKwh
		d1 := []byte(batasKwh)
		err = ioutil.WriteFile("bataskwh", d1, 0644)
		var resp string
		if err != nil {
			resp = "Kesalahan di lokal perangkat untuk perintah set batas pemakaian daya , code " + err.Error()
			return resp
		}
		LimitKwh, _ = strconv.Atoi(batasKwh)
		resp = "berhasil Set Batas perangkat " + namaPerangkat + " sebesar " + batasKwh + " kwh "
		return resp
	})
	c.On("tele_cmd_3", func(h *gosocketio.Channel, msgTele interface{}) string {
		var teleFormat teleFormatCmd3
		log.Println(msgTele)
		msgByte := msgTele.(map[string]interface{})
		teleFormat.NamaPerangkat = msgByte["namaPerangkat"].(string)
		teleFormat.Command = msgByte["command"].(string)
		namaPerangkat := teleFormat.NamaPerangkat
		cmdOnOrOff := teleFormat.Command
		cmdOnOrOff = strings.ToLower(cmdOnOrOff)
		var responTele string
		if cmdOnOrOff != "on" && cmdOnOrOff != "off" {
			responTele := "format command 3 tidak sesuai "
			return responTele
		}
		// log.Println(ghw.CPUInfo.String())
		// gopsutil.os
		platform, family, version, err := host.PlatformInformation()
		cpuk, _ := cpu.Info()
		model := strings.ToLower(cpuk[0].ModelName)
		if err == nil {
			if strings.Contains(model, "arm") {
				// log.Print(" raspberry , cmd :")
				// log.Println(valCmd)
				valCmd1 := strings.ToLower(cmdOnOrOff)
				if valCmd1 == "on" {
					RpiSetOn()
					responTele = "berhasil mengirim perintah  " + cmdOnOrOff + " di alat " + namaPerangkat
				} else if valCmd1 == "off" {
					RpiSetOff()
					responTele = "berhasil mengirim perintah  " + cmdOnOrOff + " di alat " + namaPerangkat
				} else {
					log.Println("cmd not found")
				}
			} else {
				log.Print("not raspberry , cmd :")
				log.Println(cmdOnOrOff)
				responTele = namaPerangkat + "  tidak support kendali on / off"
			}
			// log.Println(platform)
			// log.Println(family)
			// log.Println(version)
		}
		_ = platform
		_ = family
		_ = version
		return responTele
	})
	err = c.On(gosocketio.OnDisconnection, func(h *gosocketio.Channel) {
		fmt.Println("Disconnected")
	})
	// c.Join("chat")
	IoClient = c
	return c, err
}
func SockeClientSend(event string, msg string) error {
	// fmt.Println(string(data))
	// "update_modbus"
	err := IoClient.Channel.Emit(event, (msg))
	return err
}

func SocketSendDataModbus(data []byte) error {
	err := IoClient.Channel.Emit("update_modbus", string(data))
	return err
}
func teleParseMsg(txtInbox string) (msgResp string, err error) {
	msgResp = "Command invalid"
	if strings.Contains(txtInbox, "1##") {
		msgResp, err = cmd1CekPemakaianListrik(txtInbox)
	} else if strings.Contains(txtInbox, "2##") {
		msgResp, err = cmd2SetBatasPemakaian(txtInbox)
	} else if strings.Contains(txtInbox, "3##") {
		msgResp, err = cmd2OnOffPerangkat(txtInbox)
	}
	return msgResp, nil
}

// command dari telegram untuk cek pemakaian listrik
func cmd1CekPemakaianListrik(txtInbox string) (msgTxt string, err error) {
	txtInbox = strings.Replace(txtInbox, "1##", "", -1)
	msgArray := strings.Split(txtInbox, "#")
	namaPerangkat := msgArray[0]
	waktuAwal := msgArray[1]
	waktuAkhir := msgArray[2]
	// cek date
	awal, _ := fmtdate.NewTimeDate("DD-MM-YYYY hh:mm:ss", waktuAwal+" 00:00:00")
	akhir, _ := fmtdate.NewTimeDate("DD-MM-YYYY hh:mm:ss", waktuAkhir+" 23:59:59")
	valKwh, err := GetKwh(awal.Time, akhir.Time)
	msgTxt = "Pemakaian daya listrik untuk perangkat " + namaPerangkat + " dari tanggal " + waktuAwal + " s/d " + waktuAkhir + " adalah sebesar " + valKwh + " kwh"
	return msgTxt, err
}
func cmd2SetBatasPemakaian(msgTele string) (msgTxt string, err error) {
	txtInbox := msgTele
	txtInboxReplaced := strings.Replace(txtInbox, "2##", "", -1)
	msgArray := strings.Split(txtInboxReplaced, "#")
	namaPerangkat := msgArray[0]
	batasKwh := msgArray[1]
	d1 := []byte(batasKwh)
	err = ioutil.WriteFile("bataskwh", d1, 0644)
	log.Println(err)
	LimitKwh, _ = strconv.Atoi(batasKwh)
	msgTxt = "berhasil Set Batas perangkat " + namaPerangkat + " sebesar " + batasKwh + " kwh "
	return msgTxt, err
}
func cmd2OnOffPerangkat(msgTele string) (msgTxt string, err error) {
	txtInbox := msgTele
	txtInboxReplaced := strings.Replace(txtInbox, "3##", "", -1)
	msgArray := strings.Split(txtInboxReplaced, "#")
	if len(msgArray) != 2 {
		msgTxt := "format command 2 tidak sesuai"
		return msgTxt, nil
	}
	namaPerangkat := msgArray[0]
	cmdOnOrOff := msgArray[1]
	cmdOnOrOff = strings.ToLower(cmdOnOrOff)
	if cmdOnOrOff != "on" && cmdOnOrOff != "off" {
		msgTxt := "format command 3 tidak sesuai "
		return msgTxt, nil
	}
	// log.Println(ghw.CPUInfo.String())
	// gopsutil.os
	platform, family, version, err := host.PlatformInformation()
	cpuk, _ := cpu.Info()
	model := strings.ToLower(cpuk[0].ModelName)
	if err == nil {
		if strings.Contains(model, "arm") {
			// log.Print(" raspberry , cmd :")
			// log.Println(valCmd)
			valCmd1 := strings.ToLower(cmdOnOrOff)
			if valCmd1 == "on" {
				RpiSetOn()
				msgTxt = "berhasil mengirim perintah  " + cmdOnOrOff + " di alat " + namaPerangkat
			} else if valCmd1 == "off" {
				RpiSetOff()
				msgTxt = "berhasil mengirim perintah  " + cmdOnOrOff + " di alat " + namaPerangkat
			} else {
				log.Println("cmd not found")
			}
		} else {
			log.Print("not raspberry , cmd :")
			log.Println(cmdOnOrOff)
			msgTxt = namaPerangkat + "  tidak support kendali on / off"
		}
		// log.Println(platform)
		// log.Println(family)
		// log.Println(version)
	}
	_ = platform
	_ = family
	_ = version
	return msgTxt, err
}
